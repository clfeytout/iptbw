# iptbw

## Description

This package provides components, that can be used into ipyvue or ipyvuetify to build a web interface [Voilà](https://voila.readthedocs.io/en/stable/index.html#) or [Panel](https://panel.holoviz.org/index.html) for example.

## Installation

```bash
git clone git@gitlab.inria.fr:cfeytout/iptbw.git
cd iptbw
pip install -e .
```
