from setuptools import setup

setup(
    name='iptbw',
    version='0.1.0',    
    description='A package to create interactive widget in Panel with ipyvue and traitlets.',
    url='',
    author='Clément Feytout',
    author_email='clementfeytout@etu.u-bordeaux.fr',
    license='BSD 2-clause',
    packages=['iptbw'],
    install_requires=['ipyvue', 'traitlets'],

    classifiers=[],
)