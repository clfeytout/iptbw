import os

def read_component(name):
    path = os.path.join(os.path.dirname(__file__), 'components', f'{name}.vue')
    with open(path, 'r') as f:
        return f.read()